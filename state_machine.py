from states import States
from led_states import LedStates
import br_timer
from button_control import ButtonControl

class Robot:
    def __init__(self,ticker_number, main_frequency):
        self.ticker_number = ticker_number
        self.main_frequency = main_frequency
        self.main_ticker = None
        self.led_states = LedStates()
        self.last_state = None
        self.state = States.RED

        self.state_machine = {States.OFF: LedStates.off,
                              States.RED: LedStates.red,
                              States.ORANGE: LedStates.orange,
                              States.GREEN: LedStates.green}
                              
        
    def run(self):
        self.last_state, self.state = self.state_machine[self.state](self.led_states,self.last_state,self.state)

    def start(self):
        self.main_ticker = br_timer.ticker(self.ticker_number,self.main_frequency,self.run,True)
        self.main_ticker.start()
    
    def stop(self):
        self.main_ticker.stop()





