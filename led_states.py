from states import States
from machine import Pin
from random import getrandbits

class LedStates:
    def __init__(self):
        self.leds = {"red": Pin("B0",Pin.OUT),
                     "orange": Pin("E1",Pin.OUT),
                     "green": Pin("B14",Pin.OUT)}

    def all_off(self):
        for p in self.leds.values():
            p.value(0)
    
    def toggle_led(self,key):
        led = self.leds[key]
        if led.value():
            led.value(1)
        else:
            led.value(0)
        
    def off(self,last_state,state):
        self.all_off()
        print("off")
        return last_state, state

    def red(self,last_state,state):
        # entry action
        if state != last_state:
            self.all_off()
            last_state = state
        # action
        self.toggle_led("red")

        print("red")
        # state guard
        if getrandbits(4) <= 8:
            state = States.ORANGE
        return last_state, state

    def orange(self,last_state,state):
        if state != last_state:
            self.all_off()
            last_state = state
        print("orange")
        # state guard
        if getrandbits(4) <= 8:
            state = States.GREEN
        return last_state, state

    def green(self,last_state,state):
        if state != last_state:
            self.all_off()
            last_state = state
        print("green")
        # state guard
        if getrandbits(4) <= 8:
            state = States.RED
        return last_state, state

    
        