import time
import multiprocessing


class ticker(multiprocessing.Process):
    """
    Class to fake a micropython ticker on a separate process.
    Will call your callback every 1 / frequency seconds.
    """

    def __init__(self, ticker_number, frequency, callback, GC=False):
        multiprocessing.Process.__init__(self)
        self.ticker_number = ticker_number
        self.frequency = frequency
        self.callback = callback
        self.GC = GC
        return


    def run(self):

        # Shameless infinite loop...
        while True:

            t_start = time.time()
            self.callback()

            # Check if runtime did not execute frequency
            if time.time() - t_start > (1 / self.frequency):
                print('WARNING: execution time exceeded desired clock time.')

            # Hang for any remaining time
            while time.time() - t_start < (1 / self.frequency):
                time.sleep(1 / 10**6)


        return


    def stop(self):
        self.terminate()
        self.join()
        return
