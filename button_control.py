from machine import Pin
from states import States

class ButtonControl:
    def __init__(self):
        self.is_pressed = False
        self.button = Pin("C13", Pin.IN)
        self.button.irq(self.callback,Pin.IRQ_RISING)

    def callback(self,pin):
        self.is_pressed = True

    def button_state_change(self, state):
        if self.is_pressed:
            self.is_pressed = False
            if state == States.OFF:
                return States.RED
            else:
                return state.OFF
        else:
            return state

